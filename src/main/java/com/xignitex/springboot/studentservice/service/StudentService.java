package com.xignitex.springboot.studentservice.service;

import com.xignitex.springboot.studentservice.model.Student;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentService {

    private static List<Student> students = new ArrayList<>();

    static {
        Student oskar = new Student("1", "Oskar Albers", "This is the first student");
        Student remco = new Student("2", "Remco Albers", "This is the second student");

        students.add(remco);
        students.add(oskar);

    }


    public List<Student> retrieveStudents(){
            return students;
    }
}
