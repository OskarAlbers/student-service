package com.xignitex.springboot.studentservice.controller;

import com.xignitex.springboot.studentservice.model.Student;
import com.xignitex.springboot.studentservice.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/")
    public String home(){
        return "Hello from the service";
    }

    @GetMapping("/students")
    public List<Student> retrieveStudents(){
        return studentService.retrieveStudents();
    }
}
